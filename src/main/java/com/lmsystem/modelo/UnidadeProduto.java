package com.lmsystem.modelo;

import java.io.Serializable;

import javax.annotation.Nonnegative;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "unidade_produto")
public class UnidadeProduto implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Long pkunidade;

	@NotEmpty
	@Column(unique = true, nullable = false)
	private String unidade;

	@NotEmpty
	@Column(unique = true, nullable = false)
	private String sigla;

	@Nonnegative
	@Column(nullable = false)
	private int fator;

	public Long getPkunidade() {
		return pkunidade;
	}

	public void setPkunidade(Long pkunidade) {
		this.pkunidade = pkunidade;
	}

	public String getUnidade() {
		return unidade;
	}

	public void setUnidade(String unidade) {
		this.unidade = unidade;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public int getFator() {
		return fator;
	}

	public void setFator(int fator) {
		this.fator = fator;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pkunidade == null) ? 0 : pkunidade.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UnidadeProduto other = (UnidadeProduto) obj;
		if (pkunidade == null) {
			if (other.pkunidade != null)
				return false;
		} else if (!pkunidade.equals(other.pkunidade))
			return false;
		return true;
	}

}
