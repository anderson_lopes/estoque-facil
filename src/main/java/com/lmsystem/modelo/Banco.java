package com.lmsystem.modelo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "banco")
public class Banco implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private long pkbanco;

	@NotEmpty
	@Column(nullable = false)
	private String banco;

	private String codfebraban;

	public long getPkbanco() {
		return pkbanco;
	}

	public void setPkbanco(long pkbanco) {
		this.pkbanco = pkbanco;
	}

	public String getBanco() {
		return banco;
	}

	public void setBanco(String banco) {
		this.banco = banco;
	}

	public String getCodfebraban() {
		return codfebraban;
	}

	public void setCodfebraban(String codfebraban) {
		this.codfebraban = codfebraban;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (pkbanco ^ (pkbanco >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Banco other = (Banco) obj;
		if (pkbanco != other.pkbanco)
			return false;
		return true;
	}

}
