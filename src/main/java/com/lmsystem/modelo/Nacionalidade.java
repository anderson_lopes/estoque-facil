package com.lmsystem.modelo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "nacionalidade")
public class Nacionalidade implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private long pknacionalidade;

	@NotEmpty
	@Column(unique = true, nullable = false)
	private String nacionalidade;

	@Column(nullable = false)
	private Pais pais;

	public long getPknacionalidade() {
		return pknacionalidade;
	}

	public void setPknacionalidade(long pknacionalidade) {
		this.pknacionalidade = pknacionalidade;
	}

	public String getNacionalidade() {
		return nacionalidade;
	}

	public void setNacionalidade(String nacionalidade) {
		this.nacionalidade = nacionalidade;
	}

	public Pais getPais() {
		return pais;
	}

	public void setPais(Pais pais) {
		this.pais = pais;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (pknacionalidade ^ (pknacionalidade >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Nacionalidade other = (Nacionalidade) obj;
		if (pknacionalidade != other.pknacionalidade)
			return false;
		return true;
	}

}
