package com.lmsystem.modelo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "tipo_logradouro")
public class TipoLogradouro implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private long pktipologradouro;

	@NotEmpty
	@Column(unique = true, nullable = false)
	private String tipologradouro;

	private int codlogradouro;

	public long getPktipologradouro() {
		return pktipologradouro;
	}

	public void setPktipologradouro(long pktipologradouro) {
		this.pktipologradouro = pktipologradouro;
	}

	public String getTipologradouro() {
		return tipologradouro;
	}

	public void setTipologradouro(String tipologradouro) {
		this.tipologradouro = tipologradouro;
	}

	public int getCodlogradouro() {
		return codlogradouro;
	}

	public void setCodlogradouro(int codlogradouro) {
		this.codlogradouro = codlogradouro;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (pktipologradouro ^ (pktipologradouro >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoLogradouro other = (TipoLogradouro) obj;
		if (pktipologradouro != other.pktipologradouro)
			return false;
		return true;
	}

}
