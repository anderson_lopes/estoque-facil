package com.lmsystem.modelo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "sexo")
public class Sexo implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private long pksexo;

	@NotEmpty
	@Column(unique = true, nullable = false)
	private String sexo;

	@Column(unique = true, nullable = false)
	private char codsexo;

	public long getPksexo() {
		return pksexo;
	}

	public void setPksexo(long pksexo) {
		this.pksexo = pksexo;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public char getCodsexo() {
		return codsexo;
	}

	public void setCodsexo(char codsexo) {
		this.codsexo = codsexo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (pksexo ^ (pksexo >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Sexo other = (Sexo) obj;
		if (pksexo != other.pksexo)
			return false;
		return true;
	}

}
