package com.lmsystem.modelo;

import java.io.Serializable;

import javax.annotation.Nonnegative;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "municipio")
public class Municipio implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private long pkmunicipio;

	@NotEmpty
	@Column(unique = true, nullable = false)
	private String municipio;

	@Nonnegative
	@Column(unique = true, nullable = false)
	private int codmunicipio;

	@NotNull
	private Uf uf;

	public long getPkmunicipio() {
		return pkmunicipio;
	}

	public void setPkmunicipio(long pkmunicipio) {
		this.pkmunicipio = pkmunicipio;
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public int getCodmunicipio() {
		return codmunicipio;
	}

	public void setCodmunicipio(int codmunicipio) {
		this.codmunicipio = codmunicipio;
	}

	public Uf getUf() {
		return uf;
	}

	public void setUf(Uf uf) {
		this.uf = uf;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (pkmunicipio ^ (pkmunicipio >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Municipio other = (Municipio) obj;
		if (pkmunicipio != other.pkmunicipio)
			return false;
		return true;
	}

}
