package com.lmsystem.modelo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "grupo_produto")
public class GrupoProduto implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Long pkgrupoproduto;

	@Column(nullable = false)
	private String grupoproduto;

	public Long getPkgrupoproduto() {
		return pkgrupoproduto;
	}

	public void setPkgrupoproduto(Long pkgrupoproduto) {
		this.pkgrupoproduto = pkgrupoproduto;
	}

	public String getGrupoproduto() {
		return grupoproduto;
	}

	public void setGrupoproduto(String grupoproduto) {
		this.grupoproduto = grupoproduto;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pkgrupoproduto == null) ? 0 : pkgrupoproduto.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GrupoProduto other = (GrupoProduto) obj;
		if (pkgrupoproduto == null) {
			if (other.pkgrupoproduto != null)
				return false;
		} else if (!pkgrupoproduto.equals(other.pkgrupoproduto))
			return false;
		return true;
	}

}
