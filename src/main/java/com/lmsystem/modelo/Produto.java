package com.lmsystem.modelo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "produto")
public class Produto implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Long pkproduto;

	@Column(nullable = false)
	private String produto;

	@Column(nullable = false)
	private UnidadeProduto unidadeestoque;

	@Column(nullable = false)
	private SubgrupoProduto subgrupo;

	private int estoqueminimo;

	private int ressuprimento;

	private String localizacao;

	private double precomedio;

	private double ultimopreco;

	private int saldo;

	private byte[] foto;

	public Long getPkproduto() {
		return pkproduto;
	}

	public void setPkproduto(Long pkproduto) {
		this.pkproduto = pkproduto;
	}

	public String getProduto() {
		return produto;
	}

	public void setProduto(String produto) {
		this.produto = produto;
	}

	public UnidadeProduto getUnidadeestoque() {
		return unidadeestoque;
	}

	public void setUnidadeestoque(UnidadeProduto unidadeestoque) {
		this.unidadeestoque = unidadeestoque;
	}

	public SubgrupoProduto getSubgrupo() {
		return subgrupo;
	}

	public void setSubgrupo(SubgrupoProduto subgrupo) {
		this.subgrupo = subgrupo;
	}

	public int getEstoqueminimo() {
		return estoqueminimo;
	}

	public void setEstoqueminimo(int estoqueminimo) {
		this.estoqueminimo = estoqueminimo;
	}

	public int getRessuprimento() {
		return ressuprimento;
	}

	public void setRessuprimento(int ressuprimento) {
		this.ressuprimento = ressuprimento;
	}

	public String getLocalizacao() {
		return localizacao;
	}

	public void setLocalizacao(String localizacao) {
		this.localizacao = localizacao;
	}

	public double getPrecomedio() {
		return precomedio;
	}

	public void setPrecomedio(double precomedio) {
		this.precomedio = precomedio;
	}

	public double getUltimopreco() {
		return ultimopreco;
	}

	public void setUltimopreco(double ultimopreco) {
		this.ultimopreco = ultimopreco;
	}

	public int getSaldo() {
		return saldo;
	}

	public void setSaldo(int saldo) {
		this.saldo = saldo;
	}

	public byte[] getFoto() {
		return foto;
	}

	public void setFoto(byte[] foto) {
		this.foto = foto;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pkproduto == null) ? 0 : pkproduto.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Produto other = (Produto) obj;
		if (pkproduto == null) {
			if (other.pkproduto != null)
				return false;
		} else if (!pkproduto.equals(other.pkproduto))
			return false;
		return true;
	}

}
