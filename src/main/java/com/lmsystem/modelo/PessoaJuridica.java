package com.lmsystem.modelo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.br.CNPJ;

@Entity
@Table(name = "pessoa_juridica")
public class PessoaJuridica implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private long pkpessoajuridica;

	@Column(nullable = false)
	private Pessoa pessoa;

	private String nomefantasia;

	private String inscricaestadual;

	private String inscricamunicipal;

	@NotEmpty
	@Column(nullable = false)
	@Temporal(TemporalType.DATE)
	private Date datafundacao;

	@CNPJ
	@NotEmpty
	@Column(nullable = false)
	private String cnpj;

	private String contato;

	private String redesocial;

	private String website;

	private TipoLogradouro tipologradouro;

	private String endereco;

	private String numero;

	private String complemento;

	private Bairro bairro;

	private String cep;

	public long getPkpessoajuridica() {
		return pkpessoajuridica;
	}

	public void setPkpessoajuridica(long pkpessoajuridica) {
		this.pkpessoajuridica = pkpessoajuridica;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public String getNomefantasia() {
		return nomefantasia;
	}

	public void setNomefantasia(String nomefantasia) {
		this.nomefantasia = nomefantasia;
	}

	public String getInscricaestadual() {
		return inscricaestadual;
	}

	public void setInscricaestadual(String inscricaestadual) {
		this.inscricaestadual = inscricaestadual;
	}

	public String getInscricamunicipal() {
		return inscricamunicipal;
	}

	public void setInscricamunicipal(String inscricamunicipal) {
		this.inscricamunicipal = inscricamunicipal;
	}

	public Date getDatafundacao() {
		return datafundacao;
	}

	public void setDatafundacao(Date datafundacao) {
		this.datafundacao = datafundacao;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getContato() {
		return contato;
	}

	public void setContato(String contato) {
		this.contato = contato;
	}

	public String getRedesocial() {
		return redesocial;
	}

	public void setRedesocial(String redesocial) {
		this.redesocial = redesocial;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public TipoLogradouro getTipologradouro() {
		return tipologradouro;
	}

	public void setTipologradouro(TipoLogradouro tipologradouro) {
		this.tipologradouro = tipologradouro;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public Bairro getBairro() {
		return bairro;
	}

	public void setBairro(Bairro bairro) {
		this.bairro = bairro;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (pkpessoajuridica ^ (pkpessoajuridica >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PessoaJuridica other = (PessoaJuridica) obj;
		if (pkpessoajuridica != other.pkpessoajuridica)
			return false;
		return true;
	}

}
