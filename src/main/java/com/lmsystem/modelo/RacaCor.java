package com.lmsystem.modelo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "raca_cor")
public class RacaCor implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private long pkracacor;

	@NotEmpty
	@Column(unique = true, nullable = false)
	private String racacor;

	public long getPkracacor() {
		return pkracacor;
	}

	public void setPkracacor(long pkracacor) {
		this.pkracacor = pkracacor;
	}

	public String getRacacor() {
		return racacor;
	}

	public void setRacacor(String racacor) {
		this.racacor = racacor;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (pkracacor ^ (pkracacor >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RacaCor other = (RacaCor) obj;
		if (pkracacor != other.pkracacor)
			return false;
		return true;
	}

}
