package com.lmsystem.modelo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "operadora")
public class Operadora implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private long pkoperadora;

	@NotEmpty
	@Column(nullable = false)
	private String operadora;

	@NotEmpty
	@Column(nullable = false)
	private int codoperadora;

	public long getPkoperadora() {
		return pkoperadora;
	}

	public void setPkoperadora(long pkoperadora) {
		this.pkoperadora = pkoperadora;
	}

	public String getOperadora() {
		return operadora;
	}

	public void setOperadora(String operadora) {
		this.operadora = operadora;
	}

	public int getCodoperadora() {
		return codoperadora;
	}

	public void setCodoperadora(int codoperadora) {
		this.codoperadora = codoperadora;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (pkoperadora ^ (pkoperadora >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Operadora other = (Operadora) obj;
		if (pkoperadora != other.pkoperadora)
			return false;
		return true;
	}

}
