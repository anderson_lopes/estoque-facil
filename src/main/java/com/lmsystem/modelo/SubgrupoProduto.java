package com.lmsystem.modelo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "subgrupo_produto")
public class SubgrupoProduto implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long pksubgrupo;

	@Column(nullable = false)
	private String subgrupo;

	@Column(nullable = false)
	private GrupoProduto grupoProduto;

	public Long getPksubgrupo() {
		return pksubgrupo;
	}

	public void setPksubgrupo(Long pksubgrupo) {
		this.pksubgrupo = pksubgrupo;
	}

	public String getSubgrupo() {
		return subgrupo;
	}

	public void setSubgrupo(String subgrupo) {
		this.subgrupo = subgrupo;
	}

	public GrupoProduto getGrupoProduto() {
		return grupoProduto;
	}

	public void setGrupoProduto(GrupoProduto grupoProduto) {
		this.grupoProduto = grupoProduto;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pksubgrupo == null) ? 0 : pksubgrupo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SubgrupoProduto other = (SubgrupoProduto) obj;
		if (pksubgrupo == null) {
			if (other.pksubgrupo != null)
				return false;
		} else if (!pksubgrupo.equals(other.pksubgrupo))
			return false;
		return true;
	}

}
