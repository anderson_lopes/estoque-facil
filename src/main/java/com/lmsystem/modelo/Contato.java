package com.lmsystem.modelo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import org.hibernate.validator.constraints.Email;

@Entity
@Table(name = "contato")
public class Contato implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private long pkcontato;

	@Email
	private String email;

	@Max(value = 11)
	@Min(value = 11)
	private String residencial;

	@Max(value = 11)
	@Min(value = 11)
	private String profissional;

	private Operadora operadora1;

	@Max(value = 12)
	@Min(value = 12)
	private String celular1;

	private Operadora operadora2;

	@Max(value = 12)
	@Min(value = 12)
	private String celular2;

	public long getPkcontato() {
		return pkcontato;
	}

	public void setPkcontato(long pkcontato) {
		this.pkcontato = pkcontato;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getResidencial() {
		return residencial;
	}

	public void setResidencial(String residencial) {
		this.residencial = residencial;
	}

	public String getProfissional() {
		return profissional;
	}

	public void setProfissional(String profissional) {
		this.profissional = profissional;
	}

	public Operadora getOperadora1() {
		return operadora1;
	}

	public void setOperadora1(Operadora operadora1) {
		this.operadora1 = operadora1;
	}

	public String getCelular1() {
		return celular1;
	}

	public void setCelular1(String celular1) {
		this.celular1 = celular1;
	}

	public Operadora getOperadora2() {
		return operadora2;
	}

	public void setOperadora2(Operadora operadora2) {
		this.operadora2 = operadora2;
	}

	public String getCelular2() {
		return celular2;
	}

	public void setCelular2(String celular2) {
		this.celular2 = celular2;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (pkcontato ^ (pkcontato >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Contato other = (Contato) obj;
		if (pkcontato != other.pkcontato)
			return false;
		return true;
	}

}
