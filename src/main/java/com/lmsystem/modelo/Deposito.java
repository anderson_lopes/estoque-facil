package com.lmsystem.modelo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "deposito")
public class Deposito implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private long pkdeposito;

	@NotEmpty
	@Column(nullable = false)
	private String deposito;

	private Deposito fkdeposito;

	public long getPkdeposito() {
		return pkdeposito;
	}

	public void setPkdeposito(long pkdeposito) {
		this.pkdeposito = pkdeposito;
	}

	public String getDeposito() {
		return deposito;
	}

	public void setDeposito(String deposito) {
		this.deposito = deposito;
	}

	public Deposito getFkdeposito() {
		return fkdeposito;
	}

	public void setFkdeposito(Deposito fkdeposito) {
		this.fkdeposito = fkdeposito;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (pkdeposito ^ (pkdeposito >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Deposito other = (Deposito) obj;
		if (pkdeposito != other.pkdeposito)
			return false;
		return true;
	}

}
