package com.lmsystem.controller.estoque.cadastros.estoque;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import com.lmsystem.modelo.UnidadeProduto;

@Named
@ViewScoped
public class UnidadeProdutoBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private UnidadeProduto unidadeProduto;

	private UnidadeProduto unidadeprodutoselecionado;

	private List<UnidadeProduto> unidadeProdutos;

	private String nomeunidade;

	private String sigladaunidade;

	public void start() {
		this.setUnidadeProduto(new UnidadeProduto());
	}

	public void openWindow() {
		Map<String, Object> opcoes = new HashMap<>();
		opcoes.put("resizable", false);
		opcoes.put("dynamic", true);
		RequestContext.getCurrentInstance().openDialog("cadastros/estoque/unidade", opcoes, null);
	}

	public String clean() {
		return "unidade-produto?faces-redirect=true";
	}

	public void search() {
		// TODO Implementar localização pela descrição do produto
	}

	public UnidadeProduto getUnidadeProduto() {
		return unidadeProduto;
	}

	public void setUnidadeProduto(UnidadeProduto unidadeProduto) {
		this.unidadeProduto = unidadeProduto;
	}

	public UnidadeProduto getUnidadeprodutoselecionado() {
		return unidadeprodutoselecionado;
	}

	public void setUnidadeprodutoselecionado(UnidadeProduto unidadeprodutoselecionado) {
		this.unidadeprodutoselecionado = unidadeprodutoselecionado;
	}

	public List<UnidadeProduto> getUnidadeProdutos() {
		return unidadeProdutos;
	}

	public String getNomeunidade() {
		return nomeunidade;
	}

	public void setNomedaunidade(String nomeunidade) {
		this.nomeunidade = nomeunidade;
	}

	public String getSigladaunidade() {
		return sigladaunidade;
	}

	public void setSigladaunidade(String sigladaunidade) {
		this.sigladaunidade = sigladaunidade;
	}

}
