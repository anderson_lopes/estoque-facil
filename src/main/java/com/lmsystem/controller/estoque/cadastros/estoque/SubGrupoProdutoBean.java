package com.lmsystem.controller.estoque.cadastros.estoque;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import com.lmsystem.modelo.GrupoProduto;
import com.lmsystem.modelo.SubgrupoProduto;

@Named
@ViewScoped
public class SubGrupoProdutoBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private SubgrupoProduto subgrupoProduto;

	private SubgrupoProduto subgrupoProdutoSelecionado;

	private List<SubgrupoProduto> subgrupoProdutos;

	private List<GrupoProduto> grupoProdutos;

	private String nomesubgrupo;

	public void start() {
		this.setSubgrupoProduto(new SubgrupoProduto());
	}

	public void openWindow() {
		Map<String, Object> opcoes = new HashMap<>();
		opcoes.put("resizable", false);
		opcoes.put("dynamic", true);
		RequestContext.getCurrentInstance().openDialog("cadastros/estoque/subgrupo", opcoes, null);
	}

	public String clean() {
		return "subgrupo-produto?faces-redirect=true";
	}

	public void search() {
		// TODO Implementar localização pela descrição do produto
	}

	public SubgrupoProduto getSubgrupoProduto() {
		return subgrupoProduto;
	}

	public void setSubgrupoProduto(SubgrupoProduto subgrupoProduto) {
		this.subgrupoProduto = subgrupoProduto;
	}

	public SubgrupoProduto getSubgrupoProdutoSelecionado() {
		return subgrupoProdutoSelecionado;
	}

	public void setSubgrupoProdutoSelecionado(SubgrupoProduto subgrupoProdutoSelecionado) {
		this.subgrupoProdutoSelecionado = subgrupoProdutoSelecionado;
	}

	public List<SubgrupoProduto> getSubgrupoProdutos() {
		return subgrupoProdutos;
	}

	public void setSubgrupoProdutos(List<SubgrupoProduto> subgrupoProdutos) {
		this.subgrupoProdutos = subgrupoProdutos;
	}

	public List<GrupoProduto> getGrupoProdutos() {
		return grupoProdutos;
	}

	public void setGrupoProdutos(List<GrupoProduto> grupoProdutos) {
		this.grupoProdutos = grupoProdutos;
	}

	public String getNomesubgrupo() {
		return nomesubgrupo;
	}

	public void setNomesubgrupo(String nomesubgrupo) {
		this.nomesubgrupo = nomesubgrupo;
	}

}
