package com.lmsystem.controller.estoque.cadastros;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import com.lmsystem.modelo.Setor;

@Named
@ViewScoped
public class SetorBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Setor setor;

	private Setor setorselecionado;

	private List<Setor> setores;

	private String nomesetor;

	public void start() {
		this.setSetor(new Setor());
	}

	public void openWindow() {
		Map<String, Object> opcoes = new HashMap<>();
		opcoes.put("resizable", false);
		opcoes.put("dynamic", true);
		RequestContext.getCurrentInstance().openDialog("cadastros/setor", opcoes, null);
	}

	public String clean() {
		return "setor?faces-redirect=true";
	}

	public void search() {
		// TODO Implementar localização pela descrição do produto
	}

	public Setor getSetor() {
		return setor;
	}

	public void setSetor(Setor setor) {
		this.setor = setor;
	}

	public Setor getSetorselecionado() {
		return setorselecionado;
	}

	public void setSetorselecionado(Setor setorselecionado) {
		this.setorselecionado = setorselecionado;
	}

	public List<Setor> getSetores() {
		return setores;
	}

	public String getNomesetor() {
		return nomesetor;
	}

	public void setNomesetor(String nomesetor) {
		this.nomesetor = nomesetor;
	}

}
