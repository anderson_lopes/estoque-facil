package com.lmsystem.controller.estoque.cadastros;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import com.lmsystem.modelo.Usuario;

@Named
@ViewScoped
public class UsuarioBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Usuario usuario;

	private Usuario usuarioSelecionado;

	private String nomeusuario;

	private List<Usuario> usuarios;

	public void novousuario() {
		usuario = new Usuario();
	}

	public void adicionarUsuario() {

	}

	public void salvar() {
		FacesMessage msg = new FacesMessage("usuário salvo com sucesso!");
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	public void openWindow() {
		Map<String, Object> opcoes = new HashMap<>();
		opcoes.put("resizable", false);
		opcoes.put("dynamic", true);
		RequestContext.getCurrentInstance().openDialog("cadastros/usuario", opcoes, null);
	}

	public String clean() {
		return "usuario?faces-redirect=true";
	}

	public void search() {
		// TODO Implementar localização pela descrição do usuário
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Usuario getUsuarioSelecionado() {
		return usuarioSelecionado;
	}

	public void setUsuarioSelecionado(Usuario usuarioSelecionado) {
		this.usuarioSelecionado = usuarioSelecionado;
	}

	public String getNomeusuario() {
		return nomeusuario;
	}

	public void setNomeusuario(String nomeusuario) {
		this.nomeusuario = nomeusuario;
	}

	public List<Usuario> getUsuarios() {
		return usuarios;
	}

}
