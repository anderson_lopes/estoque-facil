package com.lmsystem.controller.estoque.cadastros.estoque;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import com.lmsystem.modelo.Produto;
import com.lmsystem.modelo.SubgrupoProduto;
import com.lmsystem.modelo.UnidadeProduto;

@Named
@ViewScoped
public class ProdutoBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Produto produto;

	private Produto produtoselecionado;

	private List<UnidadeProduto> unidadeProdutos;

	private List<SubgrupoProduto> subgrupoProdutos;

	private List<Produto> produtos;

	private String nomeproduto;

	public void start() {
		this.setProduto(new Produto());
	}

	public void openWindow() {
		Map<String, Object> opcoes = new HashMap<>();
		opcoes.put("resizable", false);
		opcoes.put("dynamic", true);
		RequestContext.getCurrentInstance().openDialog("cadastros/estoque/produto", opcoes, null);
	}

	public String clean() {
		return "produto?faces-redirect=true";
	}

	public void search() {
		// TODO Implementar localização pela descrição do produto
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public Produto getProdutoselecionado() {
		return produtoselecionado;
	}

	public void setProdutoselecionado(Produto produtoselecionado) {
		this.produtoselecionado = produtoselecionado;
	}

	public List<SubgrupoProduto> getSubgrupoProdutos() {
		return subgrupoProdutos;
	}

	public List<UnidadeProduto> getUnidadeProdutos() {
		return unidadeProdutos;
	}

	public String getNomeproduto() {
		return nomeproduto;
	}

	public void setNomeproduto(String nomeproduto) {
		this.nomeproduto = nomeproduto;
	}

	public List<Produto> getProdutos() {
		return produtos;
	}

	public void setProdutos(List<Produto> produtos) {
		this.produtos = produtos;
	}

}
