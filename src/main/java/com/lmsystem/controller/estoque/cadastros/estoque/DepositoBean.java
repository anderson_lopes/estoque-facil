package com.lmsystem.controller.estoque.cadastros.estoque;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import com.lmsystem.modelo.Deposito;

@Named
@ViewScoped
public class DepositoBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Deposito deposito;

	private List<Deposito> depositos;

	private String nomedeposito;

	public void start() {
		this.setDeposito(new Deposito());
	}

	public void openWindow() {
		Map<String, Object> opcoes = new HashMap<>();
		opcoes.put("resizable", false);
		opcoes.put("dynamic", true);
		RequestContext.getCurrentInstance().openDialog("cadastros/estoque/deposito", opcoes, null);
	}

	public String clean() {
		return "deposito?faces-redirect=true";
	}

	public void search() {
		// TODO Implementar localização pela descrição do produto
	}

	public Deposito getDeposito() {
		return deposito;
	}

	public void setDeposito(Deposito deposito) {
		this.deposito = deposito;
	}

	public List<Deposito> getDepositos() {
		return depositos;
	}

	public String getNomedeposito() {
		return nomedeposito;
	}

	public void setNomedeposito(String nomedeposito) {
		this.nomedeposito = nomedeposito;
	}

}
