package com.lmsystem.repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Conecta {

	private static EntityManagerFactory factory;
	private static EntityManager manager;

	static {
		factory = Persistence.createEntityManagerFactory("LMPU");
	}

	public static EntityManager getEntityManager() {
		if (manager == null) {
			manager = factory.createEntityManager();
			return manager;
		} else {
			return manager;
		}
	}

	public static void close() {
		factory.close();
	}

}
