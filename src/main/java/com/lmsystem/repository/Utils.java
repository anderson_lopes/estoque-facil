package com.lmsystem.repository;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

public class Utils
{
	public static Date converterData(String value)
	{
		String valor = ""; 
		if (value == null || value.equals(""))
		{
			return null;
		}
		else
		{
			if (value.trim().length() == 10)
				valor = value + " 00:00:00";
			else
				valor = value;
			try
			{
				SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
				formato.setLenient(false);
				Date data = formato.parse(valor);
				return data;
			}
			catch (Exception e)
			{
				return null;
			}
		}
	}

	public static String formatar (String texto, String posicao, String caractere, int tamanho)
	{  
		if (posicao == "E")
		{
			while (texto.length() < tamanho)
			{  
				texto = caractere + texto;  
			}
		}
		else
		{
			while (texto.length() < tamanho)
			{  
				texto = texto + caractere;  
			}
		}
		return texto;
	}

	public static String formataData(Date value)
	{
		if (value == null || value.equals(""))  
			return null;
		else
		{
			try
			{
				SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
				return formato.format(value);
			}
			catch (Exception e)
			{
				return null;
			}
		}
	}

	public static String formataHora(Date value)
	{
		if (value == null || value.equals(""))  
			return null;
		else
		{
			try
			{
				SimpleDateFormat formato = new SimpleDateFormat("HH:mm:ss");
				return formato.format(value);
			}
			catch (Exception e)
			{
				return null;
			}
		}
	}

	public static String formataDataHora(Date value)
	{
		if (value == null || value.equals(""))  
			return null;
		else
		{
			try
			{
				SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy'T'HH:mm:ss");
				return formato.format(value);
			}
			catch (Exception e)
			{
				return null;
			}
		}
	}

	public static String formataValor(String value)
	{
		if (value == null || value.equals(""))  
			return null;
		else
		{
			try
			{
				value = value.replace(".", "");
				value = value.replace(",", ".");
				return value;
			}
			catch (Exception e)
			{
				return null;
			}
		}
	}

	public static String converterValor(Double value, int decimais) throws ParseException
	{
		Locale locale = new Locale("pt", "BR");
		String formato = "";
		switch (decimais)
		{
		case 0:  
			formato = "###,###,##0";
			break;
		case 1:
			formato = "###,###,##0.0";
			break;
		case 2:
			formato = "###,###,##0.00";
			break;
		case 3:
			formato = "###,###,##0.000";
			break;
		case 4:
			formato = "###,###,##0.0000";
			break;
		case 5:
			formato = "###,###,##0.00000";
			break;
		default:
			formato = "###,###,##0.00";
			break;
		}
		DecimalFormat formatodecimal = formato == null ? new DecimalFormat("###,###,##0.00") : new DecimalFormat(formato);  
		formatodecimal.setDecimalFormatSymbols(new DecimalFormatSymbols(locale));
		formatodecimal.setGroupingSize(3);
		formatodecimal.setGroupingUsed(true);
		String resultado = formatodecimal.format(value);
		return resultado;
	}

	public static String md5(String value)
	{
		String retorno = "";
		MessageDigest md = null;
		try
		{
			md = MessageDigest.getInstance("MD5");
		}
		catch (NoSuchAlgorithmException e)
		{
			e.printStackTrace();
		}
		BigInteger hash = new BigInteger(1, md.digest(value.getBytes()));
		retorno = hash.toString(16);
		return retorno;
	}

	public static String conteudoXML(Object value)
	{
		final StringWriter out = new StringWriter();
		JAXBContext context = null;
		Marshaller marshaller = null;
		try
		{
			context = JAXBContext.newInstance(value.getClass());
			marshaller = context.createMarshaller();
			marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_ENCODING, "ISO-8859-1");
			marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
			marshaller.marshal(value, document);
			NodeList elements = document.getElementsByTagName("*");
			for (int i = 0; i < elements.getLength(); i++)
			{  
				elements.item(i).setPrefix("ans");
			}
			Transformer transformer = TransformerFactory.newInstance().newTransformer();  
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.transform(new DOMSource(document), new StreamResult(out));
		}
		catch (PropertyException e)
		{
			e.printStackTrace();
		}
		catch (JAXBException e)
		{
			e.printStackTrace();
		}
		catch (ParserConfigurationException e)
		{
			e.printStackTrace();
		}
		catch (TransformerConfigurationException e)
		{
			e.printStackTrace();
		}
		catch (TransformerFactoryConfigurationError e)
		{
			e.printStackTrace();
		}
		catch (TransformerException e)
		{
			e.printStackTrace();
		}
		return out.toString();
	}

	public static String formatarXML (String value)
	{
		String linhanao = "";
		String linhasim = "";
		linhanao = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>";
		linhasim = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>";
		value = value.replace(linhanao, linhasim);
		linhanao = "<ans:mensagemTISS xmlns:ans=\"http://www.ans.gov.br/padroes/tiss/schemas\" xmlns=\"http://www.ans.gov.br/padroes/tiss/schemas\" xmlns:ns2=\"http://www.w3.org/2000/09/xmldsig#\">";
		linhasim = "<ans:mensagemTISS xmlns:ans=\"http://www.ans.gov.br/padroes/tiss/schemas\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.ans.gov.br/padroes/tiss/schemas http://www.ans.gov.br/padroes/tiss/schemas/tissV3_02_00.xsd\">";
		value = value.replace(linhanao, linhasim);
		return value;
	}

	public static void salvarArquivo(String conteudo, String nomearquivo)
	{
		try
		{
			File arquivo = new File(nomearquivo);
			BufferedWriter saida = new BufferedWriter(new FileWriter(arquivo));
			saida.write(conteudo);
			saida.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public static String salvarArquivoXML(Object object, String nomearquivo)
	{
		final StringWriter out = new StringWriter();
		JAXBContext context = null;
		Marshaller marshaller = null;
		try
		{
			context = JAXBContext.newInstance(object.getClass());
			marshaller = context.createMarshaller();
			marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			marshaller.marshal(object, new StreamResult(out));
		}
		catch (PropertyException e)
		{
			e.printStackTrace();
		}
		catch (JAXBException e)
		{
			e.printStackTrace();
		}
		Writer writer = null;
		try
		{
			writer = new FileWriter(nomearquivo);
			marshaller.marshal(object, writer);
		}
		catch (JAXBException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				if (writer != null)
				{
					writer.close();
				}
			}
			catch (Exception e)
			{
				e.getMessage();
			}
		}
		return out.toString();
	}

	public static void getDiretorio (String diretorio)
	{
		File diretorios = new File(diretorio);
		if (!diretorios.exists())
		{
			diretorios.mkdirs();
		}
	}

	public static String getStringXml(Document xml, int espacosIdentacao){
		try {
			TransformerFactory transfac = TransformerFactory.newInstance();
			transfac.setAttribute("indent-number", new Integer(espacosIdentacao));
			Transformer trans = transfac.newTransformer();
			trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			trans.setOutputProperty(OutputKeys.INDENT, "yes");

			StringWriter sw = new StringWriter();
			StreamResult result = new StreamResult(sw);
			DOMSource source = new DOMSource(xml);
			trans.transform(source, result);
			String xmlString = sw.toString();
			return xmlString;
		}
		catch (TransformerException e) {
			e.printStackTrace();
			System.exit(0);
		}
		return null;
	}

	public static String formatarXMLWS (String value)
	{
		String linhanao = "";
		String linhasim = "";
		linhanao = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>";
		linhasim = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n"+"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\""+" xmlns:ans=\"http://www.ans.gov.br/padroes/tiss/schemas\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.ans.gov.br/padroes/tiss/schemas http://www.ans.gov.br/padroes/tiss/schemas/tissV3_02_00.xsd\">"+"\n"+"<soapenv:Header/>"+"\n"+"<soapenv:Body>";
		value = value.replace(linhanao, linhasim);
		linhanao = " xmlns:ans=\"http://www.ans.gov.br/padroes/tiss/schemas\" xmlns=\"http://www.ans.gov.br/padroes/tiss/schemas\" xmlns:ns2=\"http://www.w3.org/2000/09/xmldsig#\"";
		linhasim = "";
		value = value.replace(linhanao, linhasim);
		value += "</soapenv:Body>\n</soapenv:Envelope>";
		return value;
	}

}