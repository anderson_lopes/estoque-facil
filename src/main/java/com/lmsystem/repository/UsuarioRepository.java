package com.lmsystem.repository;

import java.io.Serializable;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.lmsystem.modelo.Usuario;

public class UsuarioRepository implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	protected EntityManager manager;
	
	public Usuario salvar(Usuario usuario){
		return manager.merge(usuario);
	}
	
	public void excluir(Usuario usuario){
		manager.remove(usuario);
	}

}
